using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Lesson
{
    [AddComponentMenu("Scripts/Lesson/AimController")]
    public class AimController : MonoBehaviour
    {

        [Header("Prefab")]
        [SerializeField]
        private GameObject prefabArrow;


        // Start is called before the first frame update
        void Start()
        {

        }

        // Reference documentation: https://docs.unity3d.com/ScriptReference/Physics.Raycast.html
        // See Order of Execution for Event Functions for information on FixedUpdate() and Update() related to physics queries
        void FixedUpdate()
        {
            // Bit shift the index of the layer (0) to get a bit mask DEFAULTS
            int layerMask = 1 << 0;

            // This would cast rays only against colliders in layer 0.
            // But instead we want to collide against everything except layer 8. The ~ operator does this, it inverts a bitmask.
            //layerMask = ~layerMask;

            RaycastHit hit;
            // Does the ray intersect any objects excluding the player layer
            if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit, Mathf.Infinity, layerMask))
            {
                Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * hit.distance, Color.red);
                Debug.Log("Did Hit");
                
                if(Input.GetKey(KeyCode.F))
                {
                    Quaternion spawnRotation = Quaternion.FromToRotation(Vector3.up, hit.normal);
                    GameObject arrow = Instantiate(prefabArrow, hit.point, spawnRotation);
                }
            }
            else
            {
                Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * 1000, Color.white);
                Debug.Log("Did not Hit");
            }
        }
    }
}
