using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace Lesson
{
    public class PlayerNavController : MonoBehaviour
    {
        [SerializeField]
        private Transform destinationTrasform;

        private NavMeshAgent _navMeshPlayer;

        // Start is called before the first frame update
        void Start()
        {
            _navMeshPlayer = GetComponent<NavMeshAgent>();
        }

        // Update is called once per frame
        void Update()
        {
            _navMeshPlayer.destination = destinationTrasform.position;
        }
    }
}
