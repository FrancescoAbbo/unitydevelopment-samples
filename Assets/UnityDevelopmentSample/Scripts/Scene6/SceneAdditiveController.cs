using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Lesson
{
    [AddComponentMenu("Scripts/Lesson/SceneAdditiveController")]
    public class SceneAdditiveController : MonoBehaviour
    {
        private bool additiveSceneLoaded = false;

        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            if(Input.GetKeyDown(KeyCode.Space) && !additiveSceneLoaded)
            {
                SceneManager.LoadScene("SampleScene_4", LoadSceneMode.Additive);
                additiveSceneLoaded = true;
            }
        }
    }
}
