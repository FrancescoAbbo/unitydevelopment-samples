using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Lesson
{
    [AddComponentMenu("Scripts/Lesson/CubeAnimationBehaviour")]
    public class CubeAnimationBehaviour : MonoBehaviour
    {
        [SerializeField]
        [Header("GameObjects")]
        private GameObject fieldCube;


        // Start is called before the first frame update
        void Start()
        {
            Debug.Log($"Name: [{fieldCube.name}] - SerializeField");

            Transform trasform = this.transform.GetChild(0);
            Debug.Log($"Name: [{trasform.name}] - Child Transform");

            GameObject goCube = GameObject.Find("Cube");
            Debug.Log($"Name: [{goCube.name}] - GameObject Find");
        }

        // Update is called once per frame
        void Update()
        {

        }

        private void FixedUpdate()
        {
            fieldCube.transform.Rotate(new Vector3(0.0f, 1.0f, 0.0f), 1);
        }
    }
}
