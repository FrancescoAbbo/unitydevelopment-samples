using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Lesson {

    [AddComponentMenu("Scripts/Course/BaseMonobehaviour")]
    public class BaseMonobehaviour : MonoBehaviour
    {
        //Documentation: https://docs.unity3d.com/ScriptReference/MonoBehaviour.html

        private const string TAG = "BaseMonobehaviour";

        private void OnValidate()
        {
            Debug.Log($"{TAG} - OnValidate");
        }

        private void OnEnable()
        {
            Debug.Log($"{TAG} - OnEnable");
        }

        // Start is called before the first frame update
        void Start()
        {
            Debug.Log($"{TAG} - Start");
        }

        // Update is called once per frame
        void Update()
        {
            Debug.Log($"{TAG} - Update");
        }

        private void LateUpdate()
        {
            Debug.Log($"{TAG} - LateUpdate");
        }

        private void FixedUpdate()
        {
            Debug.Log($"{TAG} - FixedUpdate");
        }

        private void OnDisable()
        {
            Debug.Log($"{TAG} - OnDisable");
        }

        private void OnDestroy()
        {
            Debug.Log($"{TAG} - OnDestroy");
        }
    }
}

