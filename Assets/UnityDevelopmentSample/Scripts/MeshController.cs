using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Lesson
{
    public class MeshController : MonoBehaviour
    {
        // Start is called before the first frame update
        void Start()
        {
            CreateTestMesh();
        }

        // Update is called once per frame
        void Update()
        {

        }

        private void CreateTestMesh()
        {
            GameObject gameObjectMesh = new GameObject("Test_Mesh");
            gameObjectMesh.name = "Test_Mesh";

            MeshFilter meshFilter = gameObjectMesh.AddComponent<MeshFilter>();
            MeshRenderer meshRender = gameObjectMesh.AddComponent<MeshRenderer>();

            Mesh mesh = meshFilter.mesh;

            mesh.Clear();

            Vector3 p1 = new Vector3(0, 0, 0);
            Vector3 p2 = new Vector3(0, 1, 0);
            Vector3 p3 = new Vector3(1, 1, 0);
            Vector3 p4 = new Vector3(1, 0, 0);

            mesh.vertices = new Vector3[] { p1, p2, p3, p4 };

            mesh.triangles = new int[] { 0, 1, 2, 0, 2, 3 };

            Vector2 uv1 = new Vector2(0,0);
            Vector2 uv2 = new Vector2(0, 1);
            Vector2 uv3 = new Vector2(1, 1);
            Vector2 uv4 = new Vector2(1, 0);

            mesh.uv = new Vector2[] { uv1, uv2, uv3, uv4};


        }
    }
}
