using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Lesson
{
    [AddComponentMenu("Scripts/Lesson/SpawnBullet")]
    public class SpawnBullet : MonoBehaviour
    {
        [Header("Prefab")]
        [SerializeField]
        private GameObject prefabBullet;

        [Header("Spawn params")]
        [SerializeField]
        private float numSpawn = 1;
        [SerializeField]
        private float forceSpawn = 800;

        private int counterBullet = 0;

        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            if(Input.GetKeyDown(KeyCode.Space))
            {
                for(int i=0; i < numSpawn; i++)
                {
                    GameObject bullet = Instantiate(prefabBullet, this.transform.position, Quaternion.identity);
                    //bullet.name = "bullet_" + counterBullet;
                    bullet.GetComponent<Rigidbody>().AddForce(this.transform.forward * forceSpawn);
                    counterBullet++;
                }       
            }
        }
    }
}
