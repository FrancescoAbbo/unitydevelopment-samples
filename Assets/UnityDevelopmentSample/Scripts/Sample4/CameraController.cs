using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Lesson
{
    [AddComponentMenu("Scripts/Lesson/CameraController")]
    public class CameraController : MonoBehaviour
    {
        [SerializeField]
        private float speed = 3;

        private Vector2 rotation = Vector2.zero;

        // Start is called before the first frame update
        void Start()
        {

        }

        void Update()
        {
            rotation.y += Input.GetAxis("Mouse X");
            rotation.x += -Input.GetAxis("Mouse Y");
            transform.eulerAngles = (Vector2)rotation * speed;
        }
    }
}

