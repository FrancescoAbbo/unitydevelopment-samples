using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Lesson
{
    [AddComponentMenu("Scripts/Lesson/BulletBehaviour")]
    public class BulletBehaviour : MonoBehaviour
    {

        private BulletManager bulletManager;

        // Start is called before the first frame update
        void Start()
        {
            GameObject bulletManagerObject = GameObject.FindGameObjectWithTag("GameController");
            if(!ReferenceEquals(bulletManagerObject, null))
            {
                bulletManager = bulletManagerObject.GetComponent<BulletManager>();
                //register delegate
                bulletManager.OnDestroyEvent += DestroyBullet;
                bulletManager.OnJumpEvent += JumpBullet;
            }
        }

        // Update is called once per frame
        void Update()
        {

        }

        private void OnDestroy()
        {
            //unregister delegate
            bulletManager.OnDestroyEvent -= DestroyBullet;
            bulletManager.OnJumpEvent -= JumpBullet;
        }

        #region BULLET_MANAGER_EVENTS

        private void DestroyBullet()
        {
            Destroy(gameObject);
        }

        private void JumpBullet()
        {
            GetComponent<Rigidbody>().AddForce(Vector3.up * 300);
        }

        #endregion //BULLET_MANAGER_EVENTS
    }
}

