using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Lesson
{
    [AddComponentMenu("Scripts/Lesson/BulletBehaviour")]
    public class SceneBulletController : MonoBehaviour
    {
        [SerializeField]
        private GameObject[] lights;

        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        public void ManageLights()
        {
            StartCoroutine(BlinkLights(0.5f));
        }

        public IEnumerator BlinkLights(float delay)
        {
            foreach(GameObject light in lights)
            {
                light.SetActive(false);
            }

            yield return new WaitForSeconds(delay);

            foreach (GameObject light in lights)
            {
                light.SetActive(true);
            }
        }
    }
}

