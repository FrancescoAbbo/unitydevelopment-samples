using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Lesson
{
    [AddComponentMenu("Scripts/Lesson/BulletManager")]
    public class BulletManager : MonoBehaviour
    {
        private const string TAG_BULLET = "Bullet";

        public delegate void BulletManagerDelegate();
        public BulletManagerDelegate OnDestroyEvent;
        public BulletManagerDelegate OnJumpEvent;

        [SerializeField]
        UnityEvent JumpAction;

        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

            if(Input.GetKeyDown(KeyCode.D))
            {
                OnDestroyEvent?.Invoke();
            }
            
            if(Input.GetKeyDown(KeyCode.J))
            {
               OnJumpEvent?.Invoke();
               JumpAction?.Invoke();
            }
        }

        private void FixedUpdate()
        {
            GameObject[] bullets = GameObject.FindGameObjectsWithTag(TAG_BULLET);
            Debug.Log($"Bullets in scene: {bullets.Length}");
        }

    }
}
