using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Lesson
{
    [Serializable]
    public class CubeData 
    {
        public int intValue = 0;
        public string stringValue = "Test";
        public GameObject[] arrayGameObject;

        public void Print()
        {
            Debug.Log($"IntValue={intValue}");
            Debug.Log($"StringValue={stringValue}");
            Debug.Log($"ArrayGameObject Lenght={arrayGameObject.Length}");
        }
    }

}
