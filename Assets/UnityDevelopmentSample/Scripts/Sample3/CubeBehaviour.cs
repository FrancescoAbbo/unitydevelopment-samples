using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Lesson
{
    [AddComponentMenu("Scripts/Lesson/CubeBehaviour")]
    public class CubeBehaviour : MonoBehaviour
    {
        [Header("Data")]
        [SerializeField]
        private CubeData cubeProperty;


        public CubeData Data {
            get => cubeProperty;
        }

        // Start is called before the first frame update
        void Start()
        {
            
        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}
