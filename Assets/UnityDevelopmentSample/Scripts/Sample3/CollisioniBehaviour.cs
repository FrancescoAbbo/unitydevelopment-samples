using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Lesson
{
    [AddComponentMenu("Scripts/Lesson/CollisioniBehaviour")]
    public class CollisioniBehaviour : MonoBehaviour
    {
        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            Vector3 position = this.transform.position;

            if (Input.GetKey(KeyCode.UpArrow))
            {
                position.z += 0.1f;
                this.transform.position = position;
            }
            else if(Input.GetKey(KeyCode.DownArrow))
            {
                position.z -= 0.1f;
                this.transform.position = position;
            }
        }

        private bool TryGetComponent(Collider collider, out CubeData cubeData)
        {
            GameObject gameObject = collider.gameObject;
            CubeBehaviour cubeBehaviour = gameObject.GetComponent<CubeBehaviour>();
            
            bool result = false;
            cubeData = null;

            if (!ReferenceEquals(cubeBehaviour, null))
            {
                cubeData = cubeBehaviour.Data;
                result = true;
            }

            return result;
        }

        #region TRIGGER_EVENTS

        private void OnTriggerEnter(Collider other)
        {
            Debug.Log("OnTriggerEnter", this.gameObject);

            CubeData cubeData;
            
            if(TryGetComponent(other, out cubeData))
            {
                cubeData.Print();
                Debug.Break();
            }
        }


        private void OnTriggerStay(Collider collision)
        {
            Debug.Log("OnTriggerStay", this.gameObject);
        }

        private void OnTriggerExit(Collider collision)
        {
            Debug.Log("OnTriggerExit", this.gameObject);
        }

        #endregion //TRIGGER_EVENTS
    }
}
