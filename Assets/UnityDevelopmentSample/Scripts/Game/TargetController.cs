using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Lesson
{
    [AddComponentMenu("Script/Lesson/TargetController")]
    public class TargetController : MonoBehaviour
    {
        [SerializeField]
        public int Point;

        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}
