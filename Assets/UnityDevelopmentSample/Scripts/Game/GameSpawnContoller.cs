using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Lesson
{
    public class GameSpawnContoller : MonoBehaviour
    {
        [SerializeField]
        private GameObject prefab;
        [SerializeField]
        private float force;

        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

            Debug.DrawRay(transform.position, Vector3.forward * 1000, Color.white);
            if (Input.GetKeyDown(KeyCode.Space))
            {
                GameObject bowling = Instantiate(prefab, this.transform.position, Quaternion.identity);
                bowling.GetComponent<Rigidbody>().AddForce(Vector3.forward * force);
            }
        }
    }
}
