using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Lesson
{
    [AddComponentMenu("Script/Lesson/CameraLookAt")]
    public class CameraLookAt : MonoBehaviour
    {

        [SerializeField]
        private GameObject playerTarget;

        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            this.transform.LookAt(playerTarget.transform);
        }
    }
}
