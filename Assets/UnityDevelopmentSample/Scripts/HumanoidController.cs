using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Lesson
{
    public class HumanoidController : MonoBehaviour
    {
        private Animator animator;

        enum SPEED { IDLE = 0, WALK = 1, RUN = 2 }
        SPEED currentSpeed = SPEED.IDLE;

        private void Awake()
        {
            animator = GetComponent<Animator>();
        }

        // Start is called before the first frame update
        void Start()
        {
            animator.SetInteger("speed", (int)currentSpeed);
        }

        // Update is called once per frame
        void Update()
        {
            if(Input.GetKeyDown(KeyCode.UpArrow))
            {
                IncreaseSpeed();
                animator.SetInteger("Speed", (int)currentSpeed);
            }
            else if(Input.GetKeyDown(KeyCode.DownArrow))
            {
                DecreaseSpeed();
                animator.SetInteger("Speed", (int)currentSpeed);
            }

            if(Input.GetKeyDown(KeyCode.Space))
            {
                DecreaseSpeed();
                animator.SetTrigger("Jump");
            }
        }

        private void IncreaseSpeed()
        {
            switch(currentSpeed)
            {
                case SPEED.IDLE:
                    currentSpeed = SPEED.WALK;
                    break;
                case SPEED.WALK:
                    currentSpeed = SPEED.RUN;
                    break;
                case SPEED.RUN:
                    //currentSpeed = SPEED.WALK;
                    break;
            }
        }

        private void DecreaseSpeed()
        {
            switch (currentSpeed)
            {
                case SPEED.IDLE:
                    //
                    break;
                case SPEED.WALK:
                    currentSpeed = SPEED.IDLE;
                    break;
                case SPEED.RUN:
                    currentSpeed = SPEED.WALK;
                    break;
            }
        }
    }
}
