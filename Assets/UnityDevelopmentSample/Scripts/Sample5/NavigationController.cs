using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Lesson
{
    [AddComponentMenu("Scripts/Lesson/NavigationController")]
    public class NavigationController : MonoBehaviour
    {
        [SerializeField]
        private GameObject canvasUI;

        private void OnEnable()
        {

        }

        // Start is called before the first frame update
        void Start()
        {
            NavigationController[] navationControllers = GameObject.FindObjectsOfType<NavigationController>();
            if(navationControllers.Length <= 1)
                DontDestroyOnLoad(this.gameObject);
        }

        // Update is called once per frame
        void Update()
        {

        }

        private void FixedUpdate()
        {
            if (SceneManager.GetActiveScene().name == "SampleScene_5")
            {
                canvasUI.SetActive(false);
            }
            else
            {
                canvasUI.SetActive(true);
            }
        }

        public void GoToMainSceneMenu()
        {
            SceneManager.LoadScene("SampleScene_5", LoadSceneMode.Single);
        }
    }
}
