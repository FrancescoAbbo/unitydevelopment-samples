using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


namespace Lesson
{
    [AddComponentMenu("Scripts/Lesson/MenuController")]
    public class MenuController : MonoBehaviour
    {
        // Start is called before the first frame update
        void Start()
        {
            // Read last sccene 
            if(PlayerPrefs.HasKey("LastScene"))
            {
                int lastIndexScene = PlayerPrefs.GetInt("LastScene");
                Debug.Log($"Last scene seen: {lastIndexScene}");
                //ChangeScene(PlayerPrefs.GetInt("LastScene"));
            }
        }

        // Update is called once per frame
        void Update()
        {

        }

        public void ChangeScene(int indexScene)
        {
            PlayerPrefs.SetInt("LastScene", indexScene);

            SceneManager.LoadScene(indexScene, LoadSceneMode.Single);

        }
    }
}
